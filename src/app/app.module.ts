import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';
import { ReactiveFormsModule } from '@angular/forms';

// material components
import { BrowserModule } from '@angular/platform-browser';
import { MaterialModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MdButtonModule, MdCheckboxModule, MdDialogModule, MdDatepickerModule, MdNativeDateModule, MdSnackBarModule } from '@angular/material';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';

// app router
import { AppRouterModule } from './router/router.module';
import { RightMenuDirective } from './directives/right-menu/right-menu.directive';
import { DynamicHtmlComponent } from './dynamic-html/dynamic-html.component';
import { ListingComponent } from './common/listing/listing.component';
import { PaginationComponent } from './common/listing/pagination/pagination.component';
import { OrdersComponent } from './sections/orders/orders.component';
import { ProductsComponent } from './sections/products/products.component';
import { ReportsComponent } from './sections/reports/reports.component';
import { InvoicesComponent } from './sections/invoices/invoices.component';
import { NotificationsComponent } from './sections/notifications/notifications.component';
import { ManagementComponent } from './sections/management/management.component';
import { SupportComponent } from './sections/support/support.component';
import { ActionBarComponent } from './common/listing/action-bar/action-bar.component';
import { CheckboxComponent } from './common/form/input/checkbox/checkbox.component';
import { ActionSelectComponent } from './common/listing/action-select/action-select.component';
import { EditOrderComponent } from './sections/orders/edit-order/edit-order.component';
import { InputTextComponent } from './common/form/input/input-text/input-text.component';
import { InputSelectComponent } from './common/form/input/input-select/input-select.component';
import { InputTextareaComponent } from './common/form/input/input-textarea/input-textarea.component';
import { InputRadioComponent } from './common/form/input/input-radio/input-radio.component';
import { AddProductsComponent } from './sections/products/add-products/add-products.component';
import { SortableComponent } from './common/listing/sortable/sortable.component';
import { InputVariableComponent } from './common/form/input/input-variable/input-variable.component';
import { CreditCardEditComponent } from './sections/management/credit-card-edit/credit-card-edit.component';
import { ViewInvoiceComponent } from './sections/invoices/view-invoice/view-invoice.component';
import { SetupComponent } from './sections/setup/setup.component';
import { CreateReturnComponent } from './sections/orders/create-return/create-return.component';
import { QuantitySelectComponent } from './common/listing/quantity-select/quantity-select.component';
import { ErrorNotificationComponent } from './common/notifications/error-notification/error-notification.component';
import { ErrorNotificationService } from './common/notifications/error-notification/error-notification.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    RightMenuDirective,
    DynamicHtmlComponent,
    ListingComponent,
    PaginationComponent,
    OrdersComponent,
    ProductsComponent,
    ReportsComponent,
    InvoicesComponent,
    NotificationsComponent,
    ManagementComponent,
    SupportComponent,
    ActionBarComponent,
    CheckboxComponent,
    ActionSelectComponent,
    EditOrderComponent,
    InputTextComponent,
    InputSelectComponent,
    InputTextareaComponent,
    InputRadioComponent,
    AddProductsComponent,
    SortableComponent,
    InputVariableComponent,
    CreditCardEditComponent,
    ViewInvoiceComponent,
    SetupComponent,
    CreateReturnComponent,
    QuantitySelectComponent,
    ErrorNotificationComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRouterModule,
    BrowserAnimationsModule,
    MdButtonModule,
    MdCheckboxModule,
    MaterialModule,
    JsonpModule,
    ReactiveFormsModule,
    MdDialogModule,
    MdDatepickerModule, 
    MdNativeDateModule,
    MdSnackBarModule
  ],
  providers: [ErrorNotificationService],
  bootstrap: [AppComponent],
  entryComponents: [CreditCardEditComponent, SetupComponent, ErrorNotificationComponent]
})
export class AppModule { }
