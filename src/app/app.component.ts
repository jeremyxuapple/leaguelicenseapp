import { Component, OnInit} from '@angular/core';
import { ActivatedRoute, Router, Params } 	from '@angular/router';
import { ApiService } from './services/ApiService';
import { MdDialog, MdDialogRef } from '@angular/material';
import { SetupComponent } from './sections/setup/setup.component';

// get a global window variable
function _window(): any {
  return window;
}

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css'],
	providers: [ApiService],
})
export class AppComponent implements OnInit {
	constructor(
		private apiService: ApiService,
		private dialog: MdDialog,
	){}

	ngOnInit() {
		this.initSetUp();
	}

	title = 'app works!';

	callApi($type) {
		this.apiService[$type]('http://leaguelicence.dev/api/v1/orders', {}).then(response => {
			console.log('response', response);
		});
	}

	initSetUp() {
		this.apiService.get(_window().storepath + '/api/v1/app/initSetup', {}).then(response => {
			// console.log('init?', (response == {}), response, response.error);
			if (response.agreement) {
				console.log('open dialog', (response == {}), response, response.error);
				// this means we have to show the pop-up for initiating the account
				let dialogRef = this.dialog.open(SetupComponent, {
					data: response,
					disableClose: true
				});
		        dialogRef.afterClosed().subscribe(result => {
					console.log('response', result);
				});
			}
		});
	}

	// define nav links
	navLinks = [
		//{ name: 'Home', url: 'home'},
		{ name: 'Sales Orders', url: 'orders'},
		{ name: 'Products', url: 'products'},
		// { name: 'Reports', url: 'reports'},
		{ name: 'Purchase Orders', url: 'invoices'},
		{ name: 'Notifications', url: 'notifications'},
		{ name: 'Account Management', url: 'management'},
		{ name: 'FAQ/Support', url: 'support'},
	]
}
