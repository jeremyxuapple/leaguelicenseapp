import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// import some components to route to
import { HomeComponent } from '../home/home.component';
import { OrdersComponent } from '../sections/orders/orders.component';
import { EditOrderComponent } from '../sections/orders/edit-order/edit-order.component';
import { CreateReturnComponent } from '../sections/orders/create-return/create-return.component';
import { ProductsComponent } from '../sections/products/products.component';
import { AddProductsComponent } from '../sections/products/add-products/add-products.component';
import { ReportsComponent } from '../sections/reports/reports.component';
import { InvoicesComponent } from '../sections/invoices/invoices.component';
import { ViewInvoiceComponent } from '../sections/invoices/view-invoice/view-invoice.component';
import { NotificationsComponent } from '../sections/notifications/notifications.component';
import { ManagementComponent } from '../sections/management/management.component';
import { SupportComponent } from '../sections/support/support.component';

// let's set the main routes here
const routes: Routes = [
    { path: '', redirectTo: '/orders', pathMatch: 'full' },
    //{ path: 'home', component: HomeComponent },
    { path: 'orders', component: OrdersComponent },
        { path: 'orders/:id/edit', component: EditOrderComponent },
        { path: 'orders/:id/return', component: CreateReturnComponent },
    { path: 'products', component: ProductsComponent },
        { path: 'products/add', component: AddProductsComponent },
    { path: 'reports', component: ReportsComponent },
    { path: 'invoices', component: InvoicesComponent },
        { path: 'invoices/:id/view', component: ViewInvoiceComponent },
    { path: 'notifications', component: NotificationsComponent },
    { path: 'management', component: ManagementComponent },
    { path: 'support', component: SupportComponent },
    { path: '**', redirectTo: '/orders'}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class AppRouterModule {

}