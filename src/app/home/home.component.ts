import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { ApiService } from '../services/ApiService';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private apiService: ApiService
  ) { }

  ngOnInit() {
      console.log('route params', this.route.snapshot.params.section);
  }

  changeRoute(route) {
    console.log('new route', route);
      //this.router.navigateByUrl(route);
  }

  callApi($type) {
    console.log('calling API', $type);
    this.apiService[$type]('http://leaguelicence.dev/api/v1/orders', {}).then(response => {
      console.log('response', response);
    });
  }
}
