import { Component, OnInit, OnChanges } from '@angular/core';
import { ApiService } from '../../services/ApiService';

// get a global window variable
function _window(): any {
  return window;
}

@Component({
	selector: 'app-orders',
	templateUrl: './orders.component.html',
	styleUrls: ['./orders.component.css'],
	providers: [ApiService],
})
export class OrdersComponent implements OnInit {

	constructor(
		private apiService: ApiService,
	) { }

	ngOnInit() {
		this.getOrders();
	}

	// let's define some variables
	dataAvailable = true;
	buffer = 0;
	orders = [];
	page = 1;
	maxpages = 1;
	limit = 10;
	query = "";
	order = undefined;
	orderOption = "order_id";

	fields = [
		// {type: 'checkbox'},
		{name: 'Date', col: 'date_created', basic: true},
		{name: 'Order ID', col: 'order_id', basic: true},
		{name: 'Customer', col: 'customer_name', basic: true},
		{name: 'Status', col: 'status', basic: true},
		{name: 'Total', col: 'total_inc_tax', currency: true},
		{name: 'Action', actions: {
				options: [
					{name: 'Edit', link: '/orders/:id/edit', replacements: {
						'id': 'id'
					}},
					{name: 'Create Return', link: '/orders/:id/return', replacements: {
						'id': 'id'
					}},
					/*{name: 'Fullfill', link: '/orders/:id/fullfill', replacements: {
						'id': 'id'
					}}*/
				]
			}
		},
	];
	// bulkOptions = [
	// 	{name: 'Delete Orders', action: 'deleteOrders'},
	// 	/*{name: 'Fullfill Orders', action: 'fulfillOrders'},*/
	// ];

	getOrders() {
		const orders = this;
		this.buffer++;
		setTimeout(function() {
			orders.buffer--;

			if (orders.buffer > 0)
				return false;

			let getvars = {
				page: orders.page,
				limit: orders.limit,
				// sortdirection: orders.sortdirection,
				order_option: orders.orderOption,
				query: '',
				order: '',
			};

			if (orders.query.length > 1)
				getvars.query = orders.query;
			else 
				delete getvars.query;

			if (typeof orders.order !== 'undefined')
				getvars.order = orders.order;
			else 
				delete getvars.order;

			orders.apiService.get(_window().storepath + '/api/v1/accounts/' + _window().userid + '/orders/', getvars).then(response => {
				if(response.count == null) { orders.dataAvailable = false; }
				orders.orders = response.orders;
				orders.page = response.page;
				orders.maxpages = response.maxpages;
				orders.limit = response.limit;
			});
		}, 50);
	}

	changePage(page: number):void {
		this.page = page;
		this.getOrders();
	}

	bulkChange(action) {
		// we have to put all orders in an array
		let selected = [];
		for(let k = 0; k < this.orders.length; k++) {
			if (this.orders[k].selected) {
				selected.push(this.orders[k]);
			}
		}

		if (typeof this[action] !== 'undefined')
			this[action](selected);
	}

	deleteOrders(orders) {
		console.log('delete orders', orders);
	}

	filterItems(query) {
		this.query = query;
		this.getOrders();
	}

	updateSort(data) {
		this.order = data.order;
		// this.sortdirection = data.direction;
		this.getOrders();
	}

}
