import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { FormBase } from '../../../common/form/form-base';
import { FormControlService } from '../../../common/form/form.service';
import { InputVariable } from '../../../common/form/input/input-variable/form.input-variable';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from '../../../services/ApiService';
import { EditOrderService } from './edit-order.service';

// get a global window variable
function _window(): any {
  return window;
}

@Component({
	selector: 'app-edit-order',
	templateUrl: './edit-order.component.html',
	styleUrls: ['./edit-order.component.css'],
	providers: [ApiService, FormControlService, EditOrderService]
})
export class EditOrderComponent implements OnInit {
	
	id: number;
	orderResponse: object;
	editable: boolean = true;
	inputs: Array<any>;
	order: FormGroup;
	products: Array<any> = [];
	shipments: Array<any> = [];
	returns: Array<any> = [];

	constructor(
		private route: ActivatedRoute,
		private apiService: ApiService,
		private fb: FormBuilder,
		private fcs: FormControlService,
		private service: EditOrderService,
		private router: Router,
	) { }

	ngOnInit() {
		this.route.params.subscribe(params => {
			this.id = params.id;
			this.getOrder();
		})
	}

	formValid = {};
	product_fields = this.service.getProductFields();
	shipping_fields = this.service.getShippingFields();
	returns_fields = this.service.getReturnsFields();

	getOrder() {
		// get the order details
		this.apiService.get(_window().storepath + '/api/v1/orders/' + this.id, undefined).then(response => {
			let data = response.order;
			// console.log(response.order);
			this.orderResponse = response.order;

			if(this.orderResponse["status"] == "Awaiting Shipment") {
				this.editable = false;
			}
			
			// some extra details for view
			data.first_name = response.order.billing_address.first_name;
			data.last_name = response.order.billing_address.last_name;
			data.email = response.order.billing_address.email;
			let fields = this.service.getFields();
			let form: FormBase<any>[] = [];
			for(let field in fields) {
				fields[field].value = data[field];
				form.push(new InputVariable(fields[field]));
			}

			this.inputs = form;
			this.order = this.fcs.toFormGroup(form);

			this.shipments = data.invoices;

			for(let r of data.returns) {
				if(r["quantity_returned"] > 0) {
					this.returns.push(r);
				}
			}
			
			this.products = data.products;
		});

		/* for dev purposes */
		const query = {
			page: 1,
			limit: 5,
		};
	}

	saveOrder() {
		// console.log('orderForm', this.order, JSON.stringify({products: this.products}));
		this.apiService.put(_window().storepath + '/api/v1/orders/'+this.id, {products: this.products}).then(response => {
			// this.router.navigateByUrl('/orders');
		});
	}

}
