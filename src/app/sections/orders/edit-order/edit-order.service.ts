import { Injectable }       from '@angular/core';

@Injectable()
export class EditOrderService {
    getFields() {
        return {
            first_name: {key: 'first_name', label: 'First Name', required: true },
            last_name: {key: 'last_name', label: 'Last Name', required: true },
            email: {key: 'email', label: 'Customer Email', required: true },
            status: {key: 'order_status', label: 'Status', required: true },
            order_id: {key: 'order_id', label: 'Order Number', required: true },   
        };
    }

    getProductFields() {
        return [
            //{type: 'checkbox', excludable: 'External Product', col: 'fulfillment'},
            {name: 'Fullfillment', col: 'fulfillment', dropdown: {options: ['Seller Licence', 'Internal']}, excludable: 'External Product' },
            {name: 'Product Sku', col: 'sku', basic: true},
            {name: 'Product Name', col: 'name', basic: true},
            {name: 'Product Details', col: 'type', basic: true},
            {name: 'Price', col: 'price_inc_tax', currency: true},
            {name: 'Quantity', col: 'quantity', basic: true},
            {name: 'Total', col: 'total_inc_tax', currency: true},
        ];
    }

    getShippingFields() {
        return [
            {name: 'Shipped On', col: 'date_created', basic: true},
            {name: 'Tracking number', col: 'tracking_number', basic: true},
            {name: 'Shipped By', col: 'carrier_company', basic: true},
            {name: 'Product SKU', col: 'sku', basic: true},
            {name: 'Product Name', col: 'product_name', basic: true},
            {name: 'Shipping Price', col: 'shipping_cost_inc_tax', currency: true},
            {name: 'Quantity Shipped', col: 'quantity', basic: true},
        ];
    }

    getReturnsFields() {
        return [
            {name: 'Product Sku', col: 'sku', basic: true},
            {name: 'Product Name', col: 'name', basic: true},
            {name: 'Quantity', col: 'quantity_returned', basic: true},
            {name: 'Reason', col: 'reason', basic: true},
            {name: 'Type', col: 'type', basic: true},
            {name: 'Status', col: 'status', basic: true},
        ];
    }
}