import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { FormBase } from '../../../common/form/form-base';
import { FormControlService } from '../../../common/form/form.service';
import { InputVariable } from '../../../common/form/input/input-variable/form.input-variable';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../../services/ApiService';

// get a global window variable
function _window(): any {
  return window;
}

@Component({
  selector: 'app-create-return',
  templateUrl: './create-return.component.html',
  styleUrls: ['./create-return.component.css'],
  providers: [ApiService, FormControlService]
})
export class CreateReturnComponent implements OnInit {

    editable: boolean = false;
    id: number;
    inputs: Array<any>;
    order: FormGroup;
    products: Array<any> = [];
    returnObjects: Array<any> = [];
    returnableProducts: Array<any> = [];
    viewableProducts: Array<any> = [];
    returnFlag: boolean = false;

    constructor(
        private route: ActivatedRoute,
        private apiService: ApiService,
        private fb: FormBuilder,
        private fcs: FormControlService,
    ) { }

    ngOnInit() {
        this.route.params.subscribe(params => {
            this.id = params.id;
            this.getOrder();
        })
    }

     fields = [
        {name: 'SKU', col: 'sku', basic: true},
        {name: 'Name', col: 'name', basic: true},
        {name: 'Qty Purchased', col: 'purchasedQty', centered: true},
        {name: 'Return Quantity', col: 'quantity_returned', quantity: true, max: 'purchasedQty'},
        {name: 'Return Reason', col: 'reason', dropdown: {options: ['Damaged','Defective','Wrong Item']}, excludable: false},
        {name: 'Return Type', col: 'type', dropdown: {options: ['Full Refund', 'Partial Refund', 'Replacement']}, excludable: false},
    ];

    getOrder() {
        // get the order details
        this.apiService.get(_window().storepath + '/api/v1/orders/' + this.id, undefined).then(response => {
            this.order = response.order;
            this.products = response.order.products;

            // verify if the order is editable
            if (this.order["status"] == "Shipped" || this.order["origin_store_status"] == "Shipped"){
                this.editable = true;
             }    
            // verify if the order already has some return request
            if(this.order["returns"].length > 0) {
                
                this.returnObjects = this.order["returns"];                
                
                for(let product of this.products ) {

                    for(let r of this.returnObjects) {
                    // findout the same product and update the corresponding quantity
                        if(r["sku"] == product["sku"]) {
                            r.purchasedQty = product.quantity;
                            if (parseFloat(r.quantity_returned) < parseFloat(product.quantity) && product["fulfillment"] == "Seller Licence") {
                                this.returnableProducts.push(r);
                            }
                            break;
                        }
                    }
                }
            }
            // the senario of no returns request before
            if(this.order["returns"].length == 0) {
                this.fields[2]["col"] = 'quantity';
                this.fields[3]["max"] = 'quantity';
                for (let p of this.products) {
                    if(p['fulfillment'] == "Seller Licence") {
                        this.returnableProducts.push(p);
                    }
                }        
            }

        });
    }

    createReturn() {
        for (let returnProduct of this.returnableProducts) {
            this.apiService.post(_window().storepath + '/api/v1/returns/', returnProduct).then(response => {
                
            });
        }
    }

}
