import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/ApiService';

// get a global window variable
function _window(): any {
  return window;
}

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit {
    constructor(
        private apiService: ApiService,
    ) { }

    ngOnInit() {
    }

    dataAvailable = true;
    buffer = 0;
    notifications = [];
    page = 1;
    maxpages = 1;
    limit = 10;
    query = "";
    order = undefined;
    sortdirection = 'asc';

    fields = [
        // {type: 'checkbox'},
        {name: 'Entity ID', col: 'entity_id', basic: true},
        {name: 'Status', col: 'status', basic: true},
        {name: 'Type', col: 'msg_type', basic: true},
        {name: 'Name', col: 'entity_name', basic: true},
        {name: 'Details', col: 'details', basic: true},
        /*{name: 'Action', actions: {
                options: [
                    {name: 'View', link: '/:type/:id', replacements: {
                        'id': 'entity_id',
                        'type': 'entity_type'
                    }}
                ]
            }
        },*/
    ];
    // bulkOptions = [
    //     {name: 'Delete Notifications', action: 'deleteNotifications'}
    // ];

    getNotifications() {
        const notifications = this;
        this.buffer++;
        setTimeout(function() {
            notifications.buffer--;

            if (notifications.buffer > 0)
                return false;

            let getvars = {
                page: notifications.page,
                limit: notifications.limit,
                sortdirection: notifications.sortdirection,
                query: '',
                order: '',
            };

            if (notifications.query.length > 1)
                getvars.query = notifications.query;
            else
                delete getvars.query;

            if (typeof notifications.order !== 'undefined')
                getvars.order = notifications.order;
            else
                delete getvars.order;


            notifications.apiService.get(_window().storepath + '/api/v1/accounts/' + _window().userid + '/notifications/', getvars).then(response => {
               if (response.count == null) notifications.dataAvailable = false;
                 
               notifications.notifications = response.notifications;
               notifications.page = response.page;
               notifications.maxpages = response.maxpages;
               notifications.limit = response.limit;
            });
        }, 50);
    }

    changePage(page: number):void {
        this.page = page;
        this.getNotifications();
    }

    bulkChange(action) {
        // we have to put all orders in an array
        let selected = [];
        for(let k = 0; k < this.notifications.length; k++) {
            if (this.notifications[k].selected) {
                selected.push(this.notifications[k]);
            }
        }

        if (typeof this[action] !== 'undefined')
            this[action](selected);
    }

    deleteNotifications(notifications) {
        const not = this;
        for(let n in notifications) {
            this.apiService.delete(_window().storepath + '/api/v1/notifications/' + notifications[n].id , {}).then(response => {

                //if (!)
                delete notifications[n];

                if (notifications == {}) {
                    not.getNotifications();
                }
            });
        }
    }

    filterItems(query) {
        this.query = query;
        this.getNotifications();
    }

    updateSort(data) {
        this.order = data.order;
        this.sortdirection = data.direction;
        this.getNotifications();
    }

}
