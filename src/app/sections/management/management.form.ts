import { Injectable }       from '@angular/core';

@Injectable()
export class ManagementForm {
    getFields() {
        return {
            product_markup: {key: 'product_markup', label: 'Product Markup Percentage', required: true },
            product_markup_update_options: {key: 'product_markup_update_options', label: 'Product Markup Update Options', required: true, options: ['Save For Furture Product Additional', 'Update All The Products Now'], controlType: 'select' },
            // currency: {key: 'currency', label: 'Currency', required: true, options: ['USD', 'CAD'], controlType: 'select' },
            currency: {key: 'currency', label: 'Currency', required: true, options: ['USD'], controlType: 'select' },
        }
    }
}