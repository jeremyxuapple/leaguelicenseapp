import { Component, OnInit } from '@angular/core';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FormArray, FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { FormBase } from '../../common/form/form-base';
import { FormControlService } from '../../common/form/form.service';
import { InputVariable } from '../../common/form/input/input-variable/form.input-variable';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../services/ApiService';
import { ManagementForm } from './management.form';
import { CreditCardEditComponent } from './credit-card-edit/credit-card-edit.component';

// get a global window variable
function _window(): any {
  return window;
}

@Component({
    selector: 'app-management',
    templateUrl: './management.component.html',
    styleUrls: ['./management.component.css'],
    providers: [ApiService, FormControlService, ManagementForm]
})
export class ManagementComponent implements OnInit {

    inputs: Array<any>;
    settings: FormGroup;
    account: any;
    creditCard: any;

    constructor(
        private route: ActivatedRoute,
        private apiService: ApiService,
        private fb: FormBuilder,
        private fcs: FormControlService,
        private service: ManagementForm,
        private dialog: MdDialog
    ) { }

    subscriptions = [
     {"level": "Contender - Package", "active": false, "desc": "Contender $39.95/month"},
     {"level": "Playoff - Package", "active": false, "desc": "Playoff $79.95/month"},
     {"level": "Championship - Package", "active": false, "desc": "Championship $99.95/month"},
    ];

    ngOnInit() {
        this.getSettings();
    }

    // getAccount() {
    //     this.apiService.get(_window().storepath + '/api/v1/accounts/' + _window().userid, {}).then(res => {
    //         console.log('account', res.account);
    //         this.account = res.account;

    //         for(let sub of this.subscriptions) {
    //             if(sub.level == this.account.active_subscription_level) {
    //                 sub.active = true;
    //             }
    //         }

    //         let fields = this.service.getFields();
    //         let form: FormBase<any>[] = [];
    //         for(let field in fields) {
    //             if (typeof res.account[field] !== 'undefined')
    //                 fields[field].value = res.account[field];
                
    //             form.push(new InputVariable(fields[field]));
    //         }

    //         this.inputs = form;
    //         this.settings = this.fcs.toFormGroup(form);
    //     });
    // }

    getSettings() {
        this.apiService.get(_window().storepath + '/api/v1/accounts/' + _window().userid, {}).then(res => {
            this.account = res.account;
            for(let sub of this.subscriptions) {
                if(sub.level == res.account.active_subscription_level) {
                    sub.active = true;
                }
            }

            let fields = this.service.getFields();
            let form: FormBase<any>[] = [];
            for(let field in fields) {
                if (typeof res.account[field] !== 'undefined')
                    fields[field].value = res.account[field];
                
                form.push(new InputVariable(fields[field]));
            }

            this.inputs = form;
            this.settings = this.fcs.toFormGroup(form);

        });
// Get the credit card information 
        this.apiService.get(_window().storepath + '/api/v1/accounts/' + _window().userid + '/subscription', {}).then(res => {
           
            this.creditCard = res.subscription;
            // console.log(this.creditCard);
        })
    }

    saveSettings() {
        
        this.account.product_markup = this.settings.value.product_markup;
        this.account.currency = this.settings.value.currency;
        this.account.product_markup_update_options = this.settings.value.product_markup_update_options;
        
        console.log(this.account);
        this.apiService.put(_window().storepath + '/api/v1/accounts/' + _window().userid, this.account).then(response => { 
            
        });
    }

    editCreditCard() {
        let dialogRef = this.dialog.open(CreditCardEditComponent, {
            //disableClose: true
            width: '80%',
            height: '80%'
        });
        dialogRef.afterClosed().subscribe(result => {
                // console.log('response', result);
            });

    }

}
