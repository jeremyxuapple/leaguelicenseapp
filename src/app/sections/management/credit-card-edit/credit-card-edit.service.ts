import { Injectable }       from '@angular/core';

@Injectable()
export class CreditCardService {
    getCardFields() {
        return {
            last_digits: {key: 'last_digits', label: 'Last Digits' },
            expiry_month: {key: 'expiry_month', label: 'Expiry Month' },
            expiry_year: {key: 'expiry_year', label: 'Expiry Year' },
            // cvv: {key: 'cvv', label: 'CVV' },
        };
    }

    getBililngFields() {
        return {
            billing_first_name: {key: 'billing_first_name', label: 'First Name', required: true },
            billing_last_name: {key: 'billing_last_name', label: 'Last Name', required: true },
            billing_company: {key: 'billing_company', label: 'Billing Company', required: true },
            billing_state: {key: 'billing_state', label: 'Billing State', required: true },
            billing_zip: {key: 'billing_zip', label: 'Billing Zip', required: true },
            billing_address: {key: 'billing_address1', label: 'Billing Address1'},
            billing_address2: {key: 'billing_address2', label: 'Billing Address2' },
            billing_city: {key: 'billing_city', label: 'Billing City', required: true },
        };
    }
}