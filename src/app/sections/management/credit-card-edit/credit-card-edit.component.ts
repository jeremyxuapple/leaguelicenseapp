import { Component, OnInit } from '@angular/core';
import { MdDialog, MdDialogRef } from '@angular/material';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiService } from '../../../services/ApiService';
import { FormBase } from '../../../common/form/form-base';
import { FormControlService } from '../../../common/form/form.service';
import { CreditCardService } from './credit-card-edit.service';
import { InputVariable } from '../../../common/form/input/input-variable/form.input-variable';

// get a global window variable
function _window(): any {
  return window;
}

@Component({
  selector: 'app-credit-card-edit',
  templateUrl: './credit-card-edit.component.html',
  styleUrls: ['./credit-card-edit.component.css'],
  providers: [ApiService, FormControlService, CreditCardService]
})
export class CreditCardEditComponent {

    creditCard: object = {};
    billinginputs: object = {};
    cardform: FormGroup;
    billingform: FormGroup;

    constructor(
        public dialogRef: MdDialogRef<CreditCardEditComponent>,
        private apiService: ApiService,
        private service: CreditCardService,
        private fcs: FormControlService,
    ) { 
        this.getCreditCard();
        this.getBilling();
    }

    getCreditCard() {
        
        this.apiService.get(_window().storepath + '/api/v1/accounts/' + _window().userid + '/subscription', {}).then(res => {
            let data = this.fcs.createForm(res.subscription, this.service.getCardFields());
            this.creditCard = data.inputs;
            this.cardform = data.form;
        });         
    }

    getBilling() {
        this.apiService.get(_window().storepath + '/api/v1/accounts/' + _window().userid, {}).then(res => {          
            let billing = this.fcs.createForm(res.account, this.service.getBililngFields());     
            this.billinginputs = billing.inputs;
            this.billingform = billing.form;
        });
    }
}
