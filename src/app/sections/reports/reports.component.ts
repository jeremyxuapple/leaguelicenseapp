import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { FormBase } from '../../common/form/form-base';
import { FormControlService } from '../../common/form/form.service';
import { InputVariable } from '../../common/form/input/input-variable/form.input-variable';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from '../../services/ApiService';
import { ReportsService } from './reports.service';

// get a global window variable
function _window(): any {
  return window;
}

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css'],
  providers: [ApiService, FormControlService, ReportsService]
})
export class ReportsComponent implements OnInit {
    id: number;
    export_inputs: Array<any>;
    orders: FormGroup;
    sales_inputs: Array<any>;
    sales: FormGroup;

    constructor(
        private route: ActivatedRoute,
        private apiService: ApiService,
        private fb: FormBuilder,
        private fcs: FormControlService,
        private service: ReportsService
    ) { }

    ngOnInit() {
        // create the export order form
        this.createForm(this.service.getExportOrderFields(), 'export_inputs', 'orders');

        // create the sales form
        this.createForm(this.service.getSalesFields(), 'sales_inputs', 'sales');
    }

    createForm(fields, inputs, form) {
        let form_fields: FormBase<any>[] = [];

        for(let field in fields) {
            fields[field].value = '';
            form_fields.push(new InputVariable(fields[field]));
        }

        this[inputs] = form_fields;
        this[form] = this.fcs.toFormGroup(form_fields);
    }

    createReport(type) {
        console.log('this', this.orders);
        let getvars = {
            date_start: this.orders.get('order_export_start_date').value,
            date_end: this.orders.get('order_export_end_date').value,
        }

        let url = _window().storepath + '/api/v1/reports/1/stores?date_start=2017-06-01&date_end=2017-07-10&type=active';
        //url = url + this.apiService.serialize(getvars, false);
        url = this.apiService.injectToken(url);
        _window().open(url);
        console.log("open", getvars, url, this.orders.value.order_export_start_date);
    }

}
