import { Injectable }       from '@angular/core';

@Injectable()
export class ReportsService {
    getExportOrderFields() {
        return {
            start_date: {key: 'order_export_start_date', label: 'Start Date', controlType: 'datepicker' },
            end_date: {key: 'order_export_end_date', label: 'End Date', controlType: 'datepicker' },
            filter: {key: 'orders_filter', label: 'Order Type', controlType: 'select', options: ['All Orders', 'Unshipped Orders'] }
        }
    }

    getSalesFields() {
        return {
            start_date: {key: 'sales_start_date', label: 'Start Date', controlType: 'datepicker' },
            end_date: {key: 'sales_end_date', label: 'End Date', controlType: 'datepicker' },
        }
    }
}