import { Component, OnInit, Inject } from '@angular/core';
import { MdDialog, MdDialogRef, MD_DIALOG_DATA } from '@angular/material';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FormBase } from '../../common/form/form-base';
import { FormControlService } from '../../common/form/form.service';
import { InputVariable } from '../../common/form/input/input-variable/form.input-variable';
import { SetupService } from './setup.service';
import { ApiService } from '../../services/ApiService';

// get a global window variable
function _window(): any {
  return window;
}

@Component({
  selector: 'app-setup',
  templateUrl: './setup.component.html',
  styleUrls: ['./setup.component.css'],
  providers: [FormControlService, SetupService, ApiService]
})
export class SetupComponent implements OnInit {

    setup: FormGroup;
    inputs: object = {};
    agreementIndex: number = 0;
    isLoading: boolean = false;
    taxid: FormGroup;
    setupOptions: FormGroup;
    constructor(
        @Inject(MD_DIALOG_DATA) public data: any,
        public dialogRef: MdDialogRef<SetupComponent>,
        private fb: FormBuilder,
        private fcs: FormControlService,
        private service: SetupService,
        private apiService: ApiService,
    ) { 
        this.createForm('taxid');
        this.createForm('setupOptions');
    }

    ngOnInit() {
        // let's get some server information
        this.apiService.get(_window().storepath + '/api/v1/app/gettest', {}).then(response => {
            console.log('response', response);
        });
    }

    createForm(id) {
        let form: FormBase<any>[] = [];
        let fields = this.service.getFields(id);
        let inputs = {};

        for(let field in fields) {
            const input = new InputVariable(fields[field]);
            form.push(input);
            inputs[fields[field].key] = input;
        }

        this.inputs[id] = inputs;
        this[id] = this.fcs.toFormGroup(form);
    }

    nextStep() {
        // let's just switch the tab to the next one
        this.agreementIndex += 1;
    }

    chooseProducts() {
        const setup = this;
        this.isLoading = true;
        let query = {
            taxid: this.fcs.toPostQuery(this.taxid, this.inputs['taxid']),
            setupOptions: this.fcs.toPostQuery(this.setupOptions, this.inputs['setupOptions']),
        };

        _window().open(_window().storepath + '/api/v1/app/productredirect?access_token=' + _window().token);
        this.apiService.post(_window().storepath + '/api/v1/app/savesetup', query).then(function() {
            console.log('redirect', _window().storepath + '/api/v1/app/productredirect?access_token=' + _window().token);
            setup.dialogRef.close(true);
        });
    }

}
