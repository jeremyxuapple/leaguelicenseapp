import { Injectable }       from '@angular/core';

@Injectable()
export class SetupService {
    getFields(id) {
        const forms = {
            taxid: {
                taxid: {key: 'taxid', label: 'Tax ID', required: true, value: 12 },
            },
            setupOptions: {
                currency: {key: 'currency', label: 'Currency', required: true, controlType: 'select', options: ['USD'], value: 'USD' },
                map_percentage: {key: 'map_percentage', label: 'MAP Percentace', required: true, value: 45 },
            }
        }
        return forms[id];
    }
}