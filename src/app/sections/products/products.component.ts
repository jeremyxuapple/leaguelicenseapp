import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../../services/ApiService';

// get a global window variable
function _window(): any {
  return window;
}

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css'],
  providers: [ApiService],
})
export class ProductsComponent implements OnInit {

  constructor(
        private apiService: ApiService,
        private router: Router,
    ) { }

    ngOnInit() {
        this.getProducts();
    }

    // let's define some variables
    dataAvailable = true;
    buffer = 0;
    products = [];
    page = 1;
    maxpages = 1;
    limit = 10;
    query = "";
    order = undefined;

    fields = [
        // {type: 'checkbox'},
        {name: 'Name', col: 'name', basic: true},
        {name: 'SKU', col: 'sku', basic: true},
        {name: 'Cost Price', col: 'price', currency: true},
        {name: 'Selling Price', col: 'customer_product_price', currency: true},
    ];
    // bulkOptions = [
    //     {name: 'Delete Products', action: 'deleteProducts'},
    // ];

    actionButtons = [
        {name: 'Add', action: 'addProduct'}
    ]

    getProducts() {
        const products = this;
        this.buffer++;
        setTimeout(function() {
            products.buffer--;

            if (products.buffer > 0)
                return false;

            let getvars = {
                page: products.page,
                limit: products.limit,
                query: '',
                order: '',
            };

            if (products.query.length > 1)
                getvars.query = products.query;
            else 
                delete getvars.query;

            if (typeof products.order !== 'undefined')
                getvars.order = products.order;
            else 
                delete getvars.order;

            products.apiService.get(_window().storepath + '/api/v1/accounts/' + _window().userid + '/products/', getvars).then(response => {
                if(response.count == null) products.dataAvailable = false; 
                products.products = response.products;
                products.page = response.page;
                products.maxpages = response.maxpages;
                products.limit = response.limit;
            });
        }, 50);
    }

    changePage(page: number):void {
        this.page = page;
        this.getProducts();
    }

    bulkChange(action) {
        // we have to put all orders in an array
        let selected = [];
        for(let k = 0; k < this.products.length; k++) {
            if (this.products[k].selected) {
                selected.push(this.products[k]);
            }
        }

        if (typeof this[action] !== 'undefined')
            this[action](selected);
    }

    doAction(action) {
        if (typeof this[action] !== 'undefined')
            this[action]();
    }

    addProduct() {
        // we're not navigating to a angular page, we're pushing to the storefront and logging in
        _window().open(_window().storepath + '/api/v1/app/productredirect?access_token=' + _window().token);
        //this.router.navigate(['/products/add']);
    }

    filterItems(query) {
        this.query = query;
        this.getProducts();
    }

    deleteProducts(products) {
        const prod = this;
        console.log('products', products);
        for(let p in products) {
            this.apiService.delete(_window().storepath + '/api/v1/products/' + products[p].product_id, {}).then(response => {
                // now let's remove this product from the list
                products.splice(p,1);

                if (products.length === 0) {
                    // let's refresh the
                    this.getProducts();
                }
            });
        }
    }

}
