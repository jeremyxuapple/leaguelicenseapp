import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/ApiService';
import { ActivatedRoute, Router } from '@angular/router';

// get a global window variable
function _window(): any {
  return window;
}


@Component({
  selector: 'app-invoices',
  templateUrl: './invoices.component.html',
  styleUrls: ['./invoices.component.css']
})
export class InvoicesComponent implements OnInit {
    constructor(
        private route: ActivatedRoute,
        private apiService: ApiService,
        private router: Router,
    ) { }

    ngOnInit() { 
        this.getInvoices();
    }
    
    dataAvailable = true;
    buffer = 0;
    invoices = [];
    page = 1;
    maxpages = 1;
    limit = 10;
    query = "";
    order = undefined;
    sortdirection = 'asc';
    sortOption = 'date_modified';
    actionbar = false;

    fields = [
        // {type: 'checkbox'},
        {name: 'Order ID', col: 'order_id', basic: true},
        {name: 'Customer', col: 'customer_name', basic: true},
        {name: 'Shipped Quantity', col: 'shipped_qty', basic: true},
        {name: 'Cost Total', col: 'cost_total', currency: true},        
        {name: 'Action', actions: {
                options: [
                    {name: 'View', link: '/invoices/:id/view', replacements: {
                        'id': 'id'
                    }}
                ]
            }
        },
    ];
    // bulkOptions = [
    //     {name: 'Delete Invoices', action: 'deleteInvoices'}
    // ];

    getInvoices() {
        const invoices = this;
        this.buffer++;
        setTimeout(function() {
            invoices.buffer--;

            if (invoices.buffer > 0)
                return false;

            let getvars = {
                page: invoices.page,
                limit: invoices.limit,
                sortdirection: invoices.sortdirection,
                order_option: invoices.sortOption,
                query: '',
                order: '',
            };

            if (invoices.query.length > 1)
                getvars.query = invoices.query;
            else
                delete getvars.query;

            if (typeof invoices.order !== 'undefined')
                getvars.order = invoices.order;
            else
                delete getvars.order;

            invoices.apiService.get(_window().storepath + '/api/v1/accounts/' + _window().userid + '/orders/', getvars).then(response => {
                if(response.count == null) invoices.dataAvailable = false;  
                
                 for(let i of response.orders) {   
                    if(i.cost_total > 0 && i.shipped_qty > 0) {  

                        invoices.invoices.push(i);
                    }
                }
           
                invoices.page = response.page;
                invoices.maxpages = response.maxpages;
                invoices.limit = response.limit;

            });
        }, 50);
    }

    changePage(page: number):void {
        this.page = page;
        // this.getInvoices();
    }

    bulkChange(action) {
        // we have to put all orders in an array
        let selected = [];
        for(let k = 0; k < this.invoices.length; k++) {
            if (this.invoices[k].selected) {
                selected.push(this.invoices[k]);
            }
        }

        if (typeof this[action] !== 'undefined')
            this[action](selected);
    }

    deleteInvoices(invoices) {
        console.log('delete invoices', invoices);
    }

    filterItems(query) {
        this.query = query;
        this.getInvoices();
    }

    updateSort(data) {
        this.order = data.order;
        this.sortdirection = data.direction;
        this.getInvoices();
    }

}
