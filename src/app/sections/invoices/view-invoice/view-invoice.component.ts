import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../../services/ApiService';
import { ActivatedRoute, Router } from '@angular/router';

// get a global window variable
function _window(): any {
  return window;
}

@Component({
  selector: 'app-view-invoice',
  templateUrl: './view-invoice.component.html',
  styleUrls: ['./view-invoice.component.css']
})
export class ViewInvoiceComponent implements OnInit {
	id: number;
  selfProduct: boolean = false;
  constructor(
  	  private route: ActivatedRoute,
      private apiService: ApiService,
      private router: Router,
  	) { }

  ngOnInit() {
   this.route.params.subscribe(params => {
          this.id = params.id;
          
      }); 
    this.getInvoice();
  }

  invoices: any;
  selfProducts = [];
  subtotal: number = 0;
  shipping_fee: number = 0;
  handling_fee: number = 0;
  net_profit: number = 0;

   fields = [
        {name: 'Product SKU', col: 'sku', basic: true},
        {name: 'Product Name', col: 'product_name', basic: true},
        {name: 'Supplier Price', col: 'supplier_price', currency: true},
        {name: 'Quantity', col: 'quantity', basic: true},
        {name: 'Total', col: 'total', currency: true},
    ];

   selfProductFields = [
      {name: 'Product SKU', col: 'sku', basic: true},
      {name: 'Product Name', col: 'name', basic: true},
      {name: 'Selling Price', col: 'price_ex_tax', currency: true},
      {name: 'Quantity', col: 'quantity', basic: true},
      {name: 'Total', col: 'total_ex_tax', currency: true},
    ];


  getInvoice() {
    const invoices = this;

  	this.apiService.get(_window().storepath + '/api/v1/orders/' + this.id, undefined).then(response => {
      invoices.invoices = response.order.invoices;
      
      for(let product of response.order.products){
        if(product.fulfillment == "Internal") {
          invoices.selfProduct = true;
          invoices.selfProducts.push(product);
        }
      }

      this.handling_fee = 4.0;

      for(let invoice of invoices.invoices) {
        this.subtotal += parseFloat(invoice.total);
        this.shipping_fee += parseFloat(invoice.shipping_cost_inc_tax);
          // for(let p of response.order.products) {
          //   if(p.fulfillment== "Seller Licence" && p.master_product_id == invoice.product_id && p.order_id == invoice.order_id) {
              
          //     this.net_profit += (parseFloat(p.price_inc_tax)-parseFloat(invoice.base_cost_price)) * parseFloat(invoice.quantity);
          //   }
          //  }
          this.net_profit += parseFloat(invoice.net_profit);  
          this.net_profit = Number(this.net_profit.toFixed(2));
        }
  	});
  }


}
