import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'quantity-select',
  templateUrl: './quantity-select.component.html',
  styleUrls: ['./quantity-select.component.css']
})
export class QuantitySelectComponent implements OnInit {
    //define inputs
    @Input() inputValue: any = 0;
    @Input() readonly: any = false;
    @Input() maxQty: any = 1;
    @Output() inputValueChange: EventEmitter<any> = new EventEmitter();
    @Input()
    get qsvalue() {
        return this.inputValue;
    }

    set qsvalue(val) {
        this.inputValue = val;
        this.inputValueChange.emit(this.inputValue);
    }


    constructor() { }

    ngOnInit() {
    }

    changeQty(increase) {
        if (increase) {
            this.qsvalue++;
        } else {
            this.qsvalue--;
        }

        if (this.qsvalue < 0) {
            this.qsvalue = 0;
        }
        // console.log(this.qsvalue, this.maxQty);

        if (this.qsvalue > this.maxQty) {
            this.qsvalue = this.maxQty;
        }
    }

}
