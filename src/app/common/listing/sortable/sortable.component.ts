import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
	selector: 'sortable',
	templateUrl: './sortable.component.html',
	styleUrls: ['./sortable.component.css']
})
export class SortableComponent implements OnInit {
	@Input() order: any = false;
    @Input() sortabledirection: any = false;
    @Input() col: string;
    @Output() updateSort: EventEmitter<any> = new EventEmitter();;

	constructor() { }

	ngOnInit() {
	}

	sortRow() {
		// console.log('sort', this.col);
		this.order = this.col;

		if (this.sortabledirection === 'asc') {
			this.sortabledirection = 'desc';
		} else {
			this.sortabledirection = 'asc';
		}

		this.updateSort.emit({order: this.order, direction: this.sortabledirection});
	}

}
