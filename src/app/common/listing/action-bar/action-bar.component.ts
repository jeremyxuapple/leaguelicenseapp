import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
	selector: 'action-bar',
	templateUrl: './action-bar.component.html',
	styleUrls: ['./action-bar.component.css']
})
export class ActionBarComponent implements OnInit {
	//define inputs
	@Input() bulkOptions: Array<any>;
	@Output() bulkOptionChange:EventEmitter<any> = new EventEmitter();
	@Output() filterItems:EventEmitter<any> = new EventEmitter();
	@Input() keywordFilter: any = true;

	constructor() { }

	ngOnInit() {
	}

	actionModel = "";
	query = "";

	confirmBulkEdit() {
		this.bulkOptionChange.emit(this.actionModel);
	}

	filter() {
		this.filterItems.emit(this.query);
	}

}
