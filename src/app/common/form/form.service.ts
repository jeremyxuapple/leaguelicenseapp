import { Injectable }   from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { InputVariable } from './input/input-variable/form.input-variable';
import { FormBase } from './form-base';

@Injectable()
export class FormControlService {
    constructor() {}

    toFormGroup(forms: FormBase<any>[]) {
        let group: any = {};

        forms.forEach(form => {
            group[form.key] = form.required ? new FormControl(form.value || '', Validators.required)
                                            : new FormControl(form.value || '');
        });

        return new FormGroup(group);
    }

    toPostQuery(form: FormGroup, inputs: any) {
        // first we loop through all the inputs
        let query = {};
        for(let field in inputs) {
            let input = inputs[field];
            let value = form.controls[input.key].value;
            query[input.key] = value;
        }

        return query;
    }

    createForm(data: any, fields: any) {
        let form: FormBase<any>[] = [];
        let inputs = {};

        // console.log('fields', fields);

        for(let field in fields) {
            if (typeof data[field] !== 'undefined')
                fields[field].value = data[field];

            const input = new InputVariable(fields[field]);
            form.push(input);
            inputs[fields[field].key] = input;
        }

        // console.log('form', form);

        return {
            form: this.toFormGroup(form),
            inputs: inputs
        }
    }
}