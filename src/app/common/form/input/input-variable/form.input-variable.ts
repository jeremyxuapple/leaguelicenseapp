import { FormBase } from '../../form-base';

export class InputVariable extends FormBase<string> {
    // default control type
    controlType = 'text';

    constructor(options: {} = {}) {
        super(options);

        for(let option in options) {
            if (options[option] !== 'undefined')
                this[option] = options[option];
        }
    }
}