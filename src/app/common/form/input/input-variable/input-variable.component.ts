import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup }        from '@angular/forms';
import { FormBase }     from '../../form-base';

@Component({
  selector: 'input-variable',
  templateUrl: './input-variable.component.html',
  styleUrls: ['./input-variable.component.css']
})
export class InputVariableComponent implements OnInit {
    @Input() input: FormBase<any>;
    @Input() form: FormGroup;
    @Input() disabled: boolean = false;
    get isValid() {
        return this.form.controls[this.input.key].valid;
    }

    ngOnInit() {
        
    }
}
