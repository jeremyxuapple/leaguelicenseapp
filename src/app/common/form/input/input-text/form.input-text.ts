import { FormBase } from '../../form-base';

export class InputText extends FormBase<string> {
    controlType = 'text';

    constructor(options: {} = {}) {
        super();
        for(let option in options) {
            this[option] = options[option];
        }
    }
}
