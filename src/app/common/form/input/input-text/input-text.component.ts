import { Component, OnInit, Input, Output, EventEmitter, DoCheck } from '@angular/core';

@Component({
	selector: 'input-text',
	templateUrl: './input-text.component.html',
	styleUrls: ['./input-text.component.css']
})
export class InputTextComponent implements DoCheck {
	// define inputs
	modelValue = '';

	// model for the input
	@Output() modelChange: EventEmitter<any> = new EventEmitter();
	@Input() 
	get model() {
		return this.modelValue;
	}

	set model(val) {
		this.modelValue = val;
		this.modelChange.emit(this.modelValue);
	}

	// model for the full form validation
	@Output() validChange: EventEmitter<any> = new EventEmitter();
	@Input()
	get valid() {
		return !this.error;
	}

	set valid(val) {
		this.validChange.emit(!this.error);
	}

	@Input() label: string;
	@Input() required: boolean = false;
	@Input() disabled: boolean = false;
	@Input() requiredMessage: string = 'This is a required field.';

	constructor() { }

	error = false;
	message = '';

	ngDoCheck() {
		this.error = false;
		this.message = '';

		// whenever this changes we have to check this input for 
		if (this.required && this.modelValue === '') {
			this.error = true;
			this.message = this.requiredMessage;
			return false;
		}
	}
}
